#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

typedef struct
{
	char dept[31];
	int nbP;
	char respAd[31];
} Departement;

typedef struct maillonDept
{
	Departement d;
	struct maillonDept *suiv;

} MaillonDept,*ListeDept;

typedef struct
{
	char nom[31];
	ListeDept lDept;
} VilleIUT;

typedef struct
{
    char ville[31];
    char dep[31];
    int decisionResp;
    int decisionCand;
} Choix;


typedef struct
{
   int numeroC;
   char nom[31];
   char prenom[31];
   float notes[4];
   float moyenne;
   int nombreChoix;
   Choix **tChoix;
} Candidat;

typedef struct maillonCand
{
    Candidat *c;
    struct maillonCand *suiv;
} MaillonCandidat, *ListeCand;

typedef struct
{
    MaillonCandidat *t;
    MaillonCandidat *q;
} FileCand;


/* Fonctions de Chargement */

int chargement(VilleIUT *tiut[],int tMax);
Departement lireDep(FILE *flot);
void enregistrement(VilleIUT *tiut[], int tLog);

/* Fonctions d'affichage */

void afficherPlace(Departement d);
void afficherDep(Departement d);
void afficherVille(VilleIUT v);
void afficherTIUT(VilleIUT *tiut[], int tLog);
void afficherVilleDep(VilleIUT v);
void afficheDeptDesIUT(VilleIUT *tiut[], int tLog);

/* Fonctions de Listes */

ListeDept listeDeptNouv(void);
ListeDept insererEntete(ListeDept lDept,Departement d);
ListeDept insererDept(ListeDept lDept, Departement d);
ListeDept supprimerEntete(ListeDept lDept);
ListeDept supprimerDept(ListeDept lDept, char *dep);
bool vide(ListeDept lDept);
int longueur(ListeDept lDept);

/* Fonctions de département */

void creerDept(VilleIUT *tiut[],int tLog);
void retirerDept(VilleIUT *tiut[], int tLog);

/* Fonctions de gestion du tableau */

int insererVille(VilleIUT *tiut[], char nomV[], int *tLog, int tMax, int pos);
int supprimerVille(VilleIUT *tiut[], char nomV[], int *tLog, int pos);

/* Fonctions de recherche */

ListeDept rechercherDept(ListeDept lDept, char dept[], int *trouve);
int rechercheIUT(VilleIUT *tiut[], int tLog, char ville[], int *trouve);

/* Fonctions de mise à jour */

void miseAJourGlobale(VilleIUT *tiut[], int tLog);
void miseAJourNomDept(Departement *d,ListeDept l);
void miseAJourResp(Departement *d);
void miseAJourPlaces(Departement *d);

void lancerPhaseCandidature(int *phaseCandidature);
// ###########################################################################################
// ###########################################################################################
// ################################ Commun au 2 parties ######################################
// ###########################################################################################
// ###########################################################################################

Candidat ** login(Candidat *tCand[],int *tMax,int *pos, int *retour);
void menuAdmin(VilleIUT *tiut[], int *tLog, int tMax,Candidat *tCand[],int tMaxC,int *phaseCandidature,FileCand *f,ListeCand *ladmis);
void menuCandidat(VilleIUT *tiut[], int *tLog, int tMax,Candidat *tCand[],int tMaxC,int posC,int phaseCandidature,FileCand *f,ListeCand *ladmis,float  mini);
void clearpage(void);
void globale(void);
void menuResp(Candidat *tCand[],int tMax,int phaseCandidature,FileCand *f,ListeCand *ladmis,float *mini);
/*Pour menuResp*/
FileCand insertFile(Candidat *tCand[],int tMax);
FileCand fileToAdmis(FileCand f, ListeCand *l, int nbAdmis,float mini);
void gererAdmis(FileCand *f, ListeCand *l,float mini);

/*autre*/
int comptClermont(Candidat *tCand[],int tMax);
// ###########################################################################################
// ###########################################################################################
// ################################ Partie II ################################################
// ###########################################################################################
// ###########################################################################################


/* Fonctions de Chargement et sauvegarde*/

Candidat ** chargementCandidats(int *tMax);
Candidat * lireCandidat(FILE *flot);
Choix * lireChoix(FILE *flot);
void sauvegarder(Candidat *tCand[], int tMax);

/* Fonctions d'affichage*/

void afficherCandidat(Candidat *c);
void afficherChoix(Choix *c);
void afficherCandChoix(Candidat *tCand[],int tMax);
void afficherCandDep(Candidat *tCand[], int tMax);

/* Fonctions de réallocation */

Candidat ** reallocationCand(Candidat *tCand[], int tMax);
Choix ** reallocationChoix(Choix *tChoix[], int nbChoix);

/* Fonctions de tri */

void triCandidats(Candidat *tCand[], int tMax);
int plusGrandCand(Candidat *tCand[], int tMax);
void echangerCand(Candidat *tCand[], int i, int j);
void triChoix(Choix *tChoix[], int nombreChoix);
int plusGrandChoix(Choix *tChoix[], int nombreChoix);
void echangerChoix(Choix *tChoix[], int i, int j);

/* Fonctions de recherche */

int rechercherChoix(Choix *tChoix[], int nombreChoix, char ville[], char dep[], int *trouve);
int rechercherCandidat(Candidat *tCand[], int tMax, int numeroC, int *trouve);

/* Fonctions d'insertion/suppresion/maj*/

Candidat ** creerCandidat(Candidat *tCand[], int *tMax,int *pos);
Choix ** insererChoix(Choix *tChoix[], int *nombreChoix);
Choix ** supprimerChoix(Choix *tChoix[], int *nombreChoix);
int miseAJourChoixCand(Choix *tChoix[], int nombreChoix);

/* Fonctions globale, menus*/
void globale2(void);
void menuCandidat2(Candidat *tCand[], int tMax);

/* Fonctions de file d'attente */

FileCand filenouv(void);
FileCand adjq(FileCand f, Candidat *c);
FileCand supt(FileCand f);
FileCand supCand(FileCand f, int numeroC);
Candidat * tete(FileCand f);
int longueurFile(FileCand f);
int positionFileAttente(FileCand f, int numeroC);
bool videFile(FileCand f);
void afficher(FileCand f);

/* Fonctions de liste d'admission */
ListeCand listeCandNouv(void);
ListeCand insererCandEntete(ListeCand lCand,Candidat *c);
ListeCand insererCand(ListeCand lCand, Candidat *c);
ListeCand supprimerCandEntete(ListeCand lCand);
ListeCand supprimerCand(ListeCand lCand, Candidat *c);
int longueurListe(ListeCand lCand);
bool videListe(ListeCand lCand);
ListeCand rechercherCandListe(ListeCand lCand, int numeroC, int *trouve);
void afficherListeCand(ListeCand l);



Candidat** remplirCandCl(Candidat *tCand[], Candidat *tempC[], int tMax, int nbCl);
void triTemp(Candidat *tCand[], int nbCl);
int plusGrandCandTemp(Candidat *tCand[], int nbCl);
void afficherCandidatsParCategorie(FileCand f,ListeCand l);
