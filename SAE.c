#include "SAE.h"

/*********************************************************** Fonctions de listes **************************************************************/
/**********************************************************************************************************************************************/
/**********************************************************************************************************************************************/

ListeDept listeDeptNouv(void) /*Permet de créer un liste vide puis la retourne à la fonction appelante.*/
{
	ListeDept lDept;
	lDept = NULL;
	return lDept;
}

ListeDept insererEntete(ListeDept lDept,Departement d) /*Permet d'insérer un MaillonDept en début d'une liste passée en paramètre puis renvoie cette même liste*/
{
	MaillonDept *m; /* Création d'un pointeur vers une structure MaillonDept */
	m = (MaillonDept *)malloc(sizeof(MaillonDept)); /* Allocation d'un espace mémoire pour le nouveau maillon */
	if(m == NULL) 
	{
		printf("Problème d'allocation mémoire lors de l'insertion\n"); /* Message d'erreur en cas de problème de malloc */
		exit(1);
	}
	m->d = d; /* Affecte le département passé en paramètre à l'attribut d du nouveau maillon */
	m->suiv = lDept;
	return m;
}

ListeDept insererDept(ListeDept lDept, Departement d) /* Permet d'insérer un maillon dans une liste donnée en paramètre dans l'ordre alpĥabétique/croissant et retourne cette liste */
{
	if(lDept == NULL) /* Si la liste est vide, insère le nouveau maillon en tête */
	{
		return insererEntete(lDept,d);
	}
	if(strcmp(d.dept, lDept->d.dept) < 0) /* Si le nom du département est inférieur à celui de la liste testé, le maillon est inséré en tête*/
	{
		return insererEntete(lDept,d);
	}
	if(strcmp(d.dept,lDept->d.dept) == 0) /* Si le maillon existe déjà, retourne la liste sans changement */
	{
		printf("Département déjà présent dans cet IUT\n");
		return lDept;
	}
	lDept->suiv = insererDept(lDept->suiv,d); /* Si aucun cas précédent n'est respecté, recommence avec le maillon suivant de la liste */
	return lDept;
}

ListeDept supprimerEntete(ListeDept lDept) /* Permet de supprimer un maillon en tête de la liste donnée en paramètre et retourne cette liste */
{
	ListeDept aux;
	if(lDept == NULL) /* Si la liste est vide, quitte le programme car cela provoquerait une erreur */
	{
		printf("Opération interdite\n");
		exit(1);
	}
	aux = lDept; /* On affecte l'adresse de la liste actuelle à une variable temporaire */
	lDept = lDept->suiv; /* On supprime le maillon */
	free(aux); /* On libère l'espace mémoire du maillon supprimer*/
	return lDept;
}

ListeDept supprimerDept(ListeDept lDept, char *dep) /* Permet de supprimer un maillon d'une liste lDept passée en paramètre à partir de son attribut nom de département (dept) et retourne cette liste */
{
	if(lDept == NULL) /* La liste est vide, on la retourne sans changements */
	{
		return lDept;
	}
	if(strcmp(dep, lDept->d.dept) < 0) /* Le maillon à supprimer n'existe pas, on retourne la liste sans changement */
	{
		return lDept;
	}
	if(strcmp(dep,lDept->d.dept) == 0) /* Le maillon à supprimer est trouvé, on le supprime */
	{
		return supprimerEntete(lDept);
	}
	lDept->suiv = supprimerDept(lDept->suiv,dep); /* Aucune des conditions précédentes n'a été respectée, on recommence avec le maillon suivant */
	return lDept;
}

int longueur(ListeDept lDept) /* Permet d'obtenir la longueur d'une liste passée en paramètre et retourne le nombre de maillons */
{
	int compt = 0; /* On déclare un compteur pour compter le nombre de maillons */
	while(lDept != NULL) /* Tant que la liste n'est pas vide, on incrémente le compteur de 1 et on passe au maillon suivant */
	{
		compt = compt + 1;
		lDept = lDept->suiv;
	}
	return compt;
}

bool vide(ListeDept lDept) /* Permet de savoir si une liste est vide et retourne un booléen */
{
	if(lDept == NULL)
		return true;
	return false;
}


/*int tete(Liste l){
	if(l==NULL){
		printf("Opération interdite\n");
		exit(1)
	}
	return l->v;
}*/

ListeDept rechercherDept(ListeDept lDept, char dept[], int *trouve)  /* Permet de recherche un maillon dans une liste de départements passée en paramètre pour ensuite renvoyer son adresse*/
{
	if(vide(lDept)) /* Si la liste est vide, la variable trouve passée en pointeur prend la valeur 0 et on retourne la liste */
	{
		*trouve = 0;
		return lDept;
	}
	if(strcmp(dept, lDept->d.dept) < 0) /* Même procédé que la condition précédente si le département recherché n'est pas trouvé*/
	{
		*trouve = 0;
		return lDept;
	}
	if(strcmp(dept,lDept->d.dept) == 0) /* Si le département recherché est trouvé, trouve prend la valeur 1 et on retourne la liste */
	{
		*trouve = 1;
		return lDept;
	}
	return rechercherDept(lDept->suiv, dept, trouve); /* Si aucune condition n'est respectée, renvoie la même recherche pour le maillon suivant */
}




/********************************************************* Fonctions d'affichage **************************************************************/
/**********************************************************************************************************************************************/
/**********************************************************************************************************************************************/


void afficherDep(Departement d) /* Permet d'afficher un département passé en paramètre de façon esthétique */
{
	printf("______________________________________________________________________________\n");
	printf("|                              Département                                   |\n");
	printf("|----------------------------------------------------------------------------|\n");
	printf("| %-32s | %3d  | %-32s |\n", d.dept, d.nbP, d.respAd);
	printf("|----------------------------------------------------------------------------|\n");
}

void afficherVille(VilleIUT v) /* Permet d'afficher le nom d'une variable de type structure villeIUT passée en paramètre de façon esthétique */
{
	printf("|----------------------------------|\n");
	printf("| %-32s |\n", v.nom);
	printf("|----------------------------------|\n");
}

void afficherTIUT(VilleIUT *tiut[], int tLog) /* Permet d'afficher le nom de toute les villes présentes dans le tableau tiut de façon esthétique*/
{
	int i = 0;
	printf("____________________________________\n");
	printf("|              Ville               |\n");
	for(i = 0; i < tLog; i++)
	{
		afficherVille(*tiut[i]);
	}
	printf("\n");
}

void afficherVilleDep(VilleIUT v) /* Permet d'afficher une ville passée en paramètre ainsi que tous les départements qui y sont présent de manière esthétique*/
{
	ListeDept l;
	printf("_________________________________________________________________________________________________________________\n");
	printf("|              Ville               |                              Département                                   |\n");
	printf("|----------------------------------|----------------------------------------------------------------------------|\n");
	l = v.lDept;
	while(l != NULL)
	{
		printf("| %-32s | %-32s | %3d | %-32s |\n", v.nom, l->d.dept, l->d.nbP, l->d.respAd);
		printf("|----------------------------------|----------------------------------------------------------------------------|\n");
		l = l->suiv;
	}
}

void afficherPlace(Departement d) /* Permet d'afficher le nombre de place disponibles dans un département passé en paramètres */
{
	printf("\nPour ce département il y a %d places en 1ère année \n\n",d.nbP);
}

void afficheDeptDesIUT(VilleIUT *tiut[], int tLog)
{
	char dept[31];
	int i,trouve;
	system("clear");
	printf("######################################################################################################################\n");
	printf("Quel département cherchez-vous dans les IUT ?\t");
	scanf("%s%*c",dept);
	printf("\n\n\t\tCe département est présent dans ces IUT :\n");
	printf("----------------------------------------------------------------------------------------------------\n");
	for(i=0;i<tLog;i++)
	{
		rechercherDept(tiut[i]->lDept,dept,&trouve);
		if (trouve==1)
		{
			printf("* %s\n",tiut[i]->nom);
		}
	}
	printf("|\n");
	clearpage();
}



/********************************************************* Fonctions de Chargement ************************************************************/
/**********************************************************************************************************************************************/
/**********************************************************************************************************************************************/


int chargement(VilleIUT *tiut[],int tMax) /* Permet de charger le contenu du fichier IUT.don dans des structures MaillonDept et VilleIUT pour remplir le tableau tiut */
{
	FILE *flot; /* Déclaration de la variable pour lire le fichier */
	int tLog = 0, pos, trouve; /* tLog, la taille logique que l'on incrémente à chaque nouvelle ville insérée dan le tableau */
	char nomV[31];
	Departement d;
	flot = fopen("IUT.don","r"); /* Ouverture du fichier */
	if(flot == NULL) /* En cas de problème avec l'ouverture du fichier, on affiche un message d'erreur, on ferme le fichier et on quitte la fonction */
	{
		printf("Erreur lors de l'ouverture du fichier\n");
		fclose(flot);
		return -1;
	}
	fscanf(flot, "%s", nomV); /* On commence par lire le nom de la première ville dans le fichier */
	while(!feof(flot))
	{
		if(tLog == tMax) /* Si le tableau est plein, on affiche un message d'erreur, on ferme le fichier et on arrête la fonction */
		{
			printf("Tableau tiut plein\n");
			fclose(flot);
			return -3;
		}
		d = lireDep(flot); /* On appelle la fonction lireDep pour remplir une structure Département à partir du fichier */
		pos = rechercheIUT(tiut,tLog,nomV,&trouve); /* On recherche la ville lue dans le tableau et on récupère sa position si elle est présente ou sa position d'insertion dans le cas contraire*/
		if(trouve == 1) /* Si la ville a été trouvée, on insère le département d lu dans la liste des départements de la ville en question*/
		{
			tiut[pos]->lDept = insererDept(tiut[pos]->lDept, d);
		}
		else /* Si la ville n'existe pas, on l'insère dans le tableau et on insère dans sa liste le département d */
		{
			insererVille(tiut, nomV, &tLog, tMax, pos);
			tiut[pos]->lDept = insererDept(tiut[pos]->lDept,d);  
		}
		fscanf(flot, "%s", nomV); /* On lit le nom de la ville suivante pour continuer le remplissage du tableau */
	}
	fclose(flot); /* On ferme le fichier avant de retourner la taille logique du tableau */
	return tLog;
}

Departement lireDep(FILE *flot) /* Permet de lire à partir d'un flot passé en paramètre les informations relatives à un département pour les insérer dans une structure Département*/
{
	Departement d;
	fscanf(flot,"%s%d", d.dept, &d.nbP);
	fgets(d.respAd,31,flot);
	d.respAd[strlen(d.respAd) - 1] = '\0';
	return d;
}



/************************************************************** Fonctions de Villes ***********************************************************/
/**********************************************************************************************************************************************/
/**********************************************************************************************************************************************/


int insererVille(VilleIUT *tiut[], char nomV[], int *tLog, int tMax, int pos) /* Permet d'insérer une ville dans le tableau tiut et renvoie un entier dépendant du déroulement de l'insertion */
{
   	int i;
   	if(*tLog == tMax) /* Si le tableau est plein, l'insertion est impossible, on affiche un message d'erreur et on retourne -1 */
   	{
   		printf("Tableau plein, insertion impossible\n");
   		return -1;
   	}
	for(i = *tLog - 1; i >= pos; i--) /* On décale tous les éléments vers la droite depuis le dernier élément du tableau jusqu'à la position d'insertion passée en paramètre */
	{
		tiut[i + 1] = tiut[i];
	}
	tiut[pos] = (VilleIUT *)malloc(sizeof(VilleIUT)); /* On alloue un espace mémoire pour la nouvelle ville à insérer */
	if(tiut[pos] == NULL) /* Si l'allocation se passe mal, on affiche un message d'erreur et on retourne -1 */
	{
		printf("Problème d'allocation mémoire lors de l'insertion de la ville\n");
		return -1;
	}
	strcpy(tiut[pos]->nom, nomV); /* On copie le nom de la ville à insérer passé en paramètre dans le nom de la structure VilleIUT allouée précédemment*/
	tiut[pos]->lDept = listeDeptNouv(); /* On créer une liste de départements vide pour la nouvelle ville */
	*tLog = *tLog + 1; /* On incrémente de 1 la taille logique après insertion de la nouvelle ville */
	return 0;
}

int supprimerVille(VilleIUT *tiut[], char nomV[], int *tLog, int pos) /* Permet de supprimer une ville du tableau tiut dont le nom est passé en paramètre*/
{
	VilleIUT *aux; /* Création d'un pointeur auxiliaire vers une structure villeIUT */
	aux = tiut[pos]; /* Attribution de la ville à supprimer à la variable auxiliaire */
	while(pos < *tLog - 1) /* Décalage à gauche de tous les éléments du tableau à partir de la position de la villeà supprimer passée en paramètre */
	{
		tiut[pos] = tiut[pos + 1];
		pos++;
	}
	while(aux->lDept != NULL) /* Suppression de tous les départements dans la liste de la ville supprimée */
	{
		aux->lDept = supprimerEntete(aux->lDept);
	}
	free(aux); /* Libération de la mémoire occupée par la ville supprimée*/
	*tLog = *tLog - 1; /* Diminution de la taille logique de 1 après la suppression de la ville */
	return 0;
}


int rechercheIUT(VilleIUT *tiut[], int tLog, char ville[], int *trouve) /* Permet d'effectuer une recherche dichotomique dans le tableau tiut d'une ville passée en paramètre */
{
	int inf,sup,t; /* Déclarations des variables permettant la navigarion dans le tableau */
	inf = 0;
	sup = tLog - 1;
	while(inf <= sup)
	{
		t = (inf + sup) / 2; /* Calcul de l'indice de position central du tableau*/
		if(strcmp(ville, tiut[t]->nom) == 0) /* Si la ville est trouvée, on donne la valeur 1 à la variable trouve passée par pointeur et on retourne sa position */
		{
			*trouve = 1;
			return t;
		}
		if(strcmp(ville, tiut[t]->nom) < 0) /* Si la ville recherchée est inférieure à la ville à la position t (centrale), on continue la recherche dans la partie gauche du tableau */
		{
			sup = t - 1;
		}
		else /* Dans le cas contraire, on continue la recherche dans la partie droite du tableau */
		{
			inf = t + 1;
		}
	}
	*trouve = 0; /* Si la ville n'a pas été trouvée, on donne la valeur 0 à trouve et on retourne sa position d'insertion inf */
	return inf;
}


/******************************************************* Fonctions de départements ************************************************************/
/**********************************************************************************************************************************************/
/**********************************************************************************************************************************************/


void creerDept(VilleIUT *tiut[],int tLog) /* Permet de demander à l'administrateur toutes les informations nécessaires pour la création d'un nouveau département et exécute cette création */
{
	Departement d;
	int pos, trouve;
	char ville[31];
	printf("Dans quelle ville souhaitez-vous créer un nouveau département ?\nSaisie : "); /* Recueil du nom de la ville dans laquelle insérer le département */
	scanf("%s%*c", ville);
	printf("\n");
	pos = rechercheIUT(tiut, tLog, ville, &trouve); /* Vérification que la ville est bien existante, si ce n'est pas le cas, on re-demande de saisir le nom tant que ce n'est pas bon */
	while(trouve == 0)
	{
		printf("Cette ville n'existe pas, veuillez re-saisir : ");
		scanf("%s%*c", ville);
		printf("\n");
		pos = rechercheIUT(tiut, tLog, ville, &trouve);
	}
	printf("Quel est le nom du département à insérer ?\nSaisie : "); /* Demande le nom du département à insérer */
	scanf("%s%*c", d.dept);
	rechercherDept(tiut[pos]->lDept, d.dept, &trouve); /* Vérification que le département n'existe pas déjà */
	if(trouve == 1) /* Si le département existe déjà, on affiche un message d'erreur et on arrête la fonction */
	{
		printf("Erreur, le département %s existe déjà dans cet IUT\n", d.dept);
		return;
	}
	printf("Combien de place y a-t-il pour dans ce département ?\nSaisie : "); /* On continue de demander les informations nécessaires sur le département à insérer */
	scanf("%d%*c", &d.nbP);
	printf("\n");
	printf("Quel est le nom du responsable du département ?\nSaisie : ");
	fgets(d.respAd, 31, stdin);
	d.respAd[strlen(d.respAd) - 1] = '\0';
	afficherDep(d); /* On affiche le département qui va être inséré */
	tiut[pos]->lDept = insererDept(tiut[pos]->lDept, d); /* On insère le département dans la liste de la ville correspondante */
	return;
}


void retirerDept(VilleIUT *tiut[], int tLog) /* Permet de gérer la suppression d'un département dans une ville saisie dans la fonction */
{
	int trouve, pos;
	char ville[31], dep[31], choix;
	printf("Dans quelle ville souhaitez-vous supprimer un département ?\nSaisie : "); /* Demande dans quelle ville on souhaite supprimer un département */
	scanf("%s%*c", ville);
	printf("\n");
	pos = rechercheIUT(tiut, tLog, ville, &trouve); /* On vérifie que la ville existe */
	while(trouve == 0) /* Si la ville n'existepas, on effectue une saisie contrôlée tant que ce n'est pas bon */
	{
		printf("Cette ville n'existe pas, veuillez re-saisir : ");
		scanf("%s%*c", ville);
		printf("\n");
		pos = rechercheIUT(tiut, tLog, ville, &trouve);
	}
	printf("Quel est le nom du département à supprimer ?\nSaisie : "); /* Demande le nom du département à supprimer */
	scanf("%s%*c", dep);
	rechercherDept(tiut[pos]->lDept, dep, &trouve); /* Vérifie que le département en question existe */
	if(trouve == 0) /* S'il n'existe pas, affiche un message d'erreur et arrête la fonction */
	{
		printf("Erreur, le département %s n'existe pas dans cet IUT\n", dep);
		return;
	}
	printf("Êtes-vous sur de vouloir supprimer ce département ? (O/n)\nSaisie : "); /* Demande confirmation de la suppression */
	scanf("%c%*c", &choix);
	printf("\n");
	if(choix == 'N' || choix == 'n') /* Si le choix et non, annule la supression */
	{
		printf("Suppression annulée\n");
		return;
	}
	tiut[pos]->lDept = supprimerDept(tiut[pos]->lDept, dep); /* Supprime le département */
	return;
}


//############################################################### fonction mise à jour ###############################################################
//####################################################################################################################################################
//####################################################################################################################################################
void miseAJourNomDept(Departement *d,ListeDept l)
{
	int trouve;
	char nomDept[31],choix;
	system("clear");
	afficherDep(*d); 
	while(choix!='o' && choix!='O')	//Boucle pour de la saisie controlée
	{
		printf("\nQuel est le nouveau nom du département?\n");	//Demande le nouveau nom du département
		fgets(nomDept,31,stdin);			//Affecte la saisie au tableau "dept"
		nomDept[strlen(nomDept)-1]='\0';
		rechercherDept(l,nomDept,&trouve); //Recherche si le nouveau nom de département saisie n'est pas déjà présent dans la liste des département de la ville
		if(trouve==1)	//Message informatif si le nouveau nom est déjà présent 
		{
			printf("Important : Nom de département déjà présent dans l'IUT.\n");
		}
		printf("Êtes-vous sûr de remplacer le nom du département par %s?(o/n)\t",nomDept);	//Demande confirmation
		scanf("%c%*c",&choix); //affecte la saisie à la variable "choix"
	}
    strcpy(d->dept,nomDept);	//Applique la modification
	afficherDep(*d);			//Affiche la modification
	clearpage();
}

void miseAJourResp(Departement *d)
{
	char resp[31],choix='n';
	system("clear");
	afficherDep(*d);
	while(choix!='o' && choix!='O')	//Boucle pour de la saisie controlée
	{
		printf("\nQuel est le nouveau nom du responsable ?\t");//Demande le nouveau nom du responsable
		fgets(resp,31,stdin);
		resp[strlen(resp)-1]='\0';
		printf("Êtes-vous sûr de remplacer le nom du responsable par %s?(o/n)\t",resp);//Demande confirmation
		scanf("%c%*c",&choix);
	}
	strcpy(d->respAd,resp);//Applique la modification
	afficherDep(*d);//Affiche la modification
	clearpage();
}

void miseAJourPlaces(Departement *d)
{
	int places;
	char choix='n';
	system("clear");
	afficherDep(*d);
	while(choix!='o' && choix!='O')//Boucle pour de la saisie controlée
	{
		printf("Quel est le nouveau nombre de place ?\t");//Demande le nouveau nombre de place
		scanf("%d%*c",&places);
		printf("Êtes-vous sûr de passer les places en première année de %d à %d ?(o/n)\t",d->nbP,places);//Demande confirmation
		scanf("%c%*c",&choix);
	}
	d->nbP=places;//Applique la modification
	afficherDep(*d);//Affiche la modification
	clearpage();
}

void miseAJourGlobale(VilleIUT *tiut[], int tLog)
{
	int trouve,pos,choix=0;			//Déclaration de variables | initialisation de "choix" pour rentrer dans la boucle
	char ville[31],dept[31];
	MaillonDept *m;
	Departement *d;
	system("clear");
	afficherTIUT(tiut,tLog);		//affichage des différentes villes
	printf("Dans quelle ville voulez-vous faire des mises à jour?\t");	//demande sur quelle ville faire une mise à jour
	scanf("%s%*c",ville);	//affecte la saisie au tableau "ville"
	pos=rechercheIUT(tiut,tLog,ville,&trouve);	//recherche de la saisie dans le tableau des villes et associe la position dans le tableau à la variable "pos"
	if(trouve!=1)		//message d'erreur si la ville n'est pas trouvée
	{
		printf("\n\nCette ville n'existe pas\n");
		clearpage();
		return;
	}
	system("clear");
	afficherVilleDep(*tiut[pos]);	//Affiche les différents départements de la ville saisie
	printf("Quel département de %s voulez-vous modifier?\t",ville); //demande sur quel département faire une mise à jour
	scanf("%s%*c",dept);	//affecte la saisie au tableau "dept"
	m=rechercherDept(tiut[pos]->lDept,dept,&trouve);	//recherche du département saisie et affecte l'adresse du maillon correspondant au pointeur "m"
	if(trouve!=1) // message d'erreur si le département n'apparait pas dans la liste
	{
		printf("\n\nCe département n'existe pas dans cette ville\n");
		clearpage();
		return;
	}
	d=&(m->d);	//affecte au pointeur de département "d" l'adresse de la structure "Departement" du maillon qui nous intéresse
	while(choix!=9)	// boucle pour le menu des mises à jour
	{
		system("clear");
		afficherDep(m->d); // affichage du département et de ses caractéristiques
		printf("\n\n");
		printf("###################################### Menu des mises à jour ######################################\n");
		printf("\n\t1.Mise à jour du nom du département\n\t2.Mise à jour des places du département\n\t3.Mise à jour du responsable du département\n\t9.Quitter\n\n"); // affiche les modifications qu'on peut faire sur le département
		printf("\tSaisie:\t");
		scanf("%d%*c",&choix); // associe la saisie à la variable choix
		if(choix==1) // comparaisons de la saisie pour savoir ce que l'utilisateur veut faire et appels des fonctions correspondantes
		{
			miseAJourNomDept(&(m->d),tiut[pos]->lDept); 
		}
		if(choix==2)
		{
			miseAJourPlaces(&(m->d));
		}
		if(choix==3)
		{
			miseAJourResp(&(m->d));
		}
	}
	afficherDep(m->d); //affichages du département après les potentielles modifications
	clearpage();
}

void lancerPhaseCandidature(int *phaseCandidature)
{
	char choix;
	system("clear");
	if (*phaseCandidature==0)
	{
		printf("La phase de candidature n'a pas été lancée.\n");
		printf("\nÊtes-vous sur de lancer la phase de candidature?(o/n)\t");
    	scanf("%c%*c",&choix);
		if(choix!='O' && choix!='o')
		{
			printf("La phase de candidature n'a pas été lancée.\n");
			clearpage();
			return;
		}
		*phaseCandidature=1;
		printf("\nLa phase de candidature a été lancée.\n");	
	}
	else if(*phaseCandidature==1)
	{
		printf("La phase de candidature est en court.\n");
		printf("\nÊtes-vous sur d'arrêter la phase de candidature?(o/n)\t");
		scanf("%c%*c",&choix);
		if(choix!='O' && choix!='o')
		{
			printf("La phase de candidature n'a pas été arrêtée.\n");
			clearpage();
			return;
		}
		*phaseCandidature=2;
		printf("\nLa phase de candidature a été arrêtée.\n");
	}
    clearpage();
}
/*autre*/
int comptClermont(Candidat *tCand[],int tMax)
{
	int i=0,compt=0,j=0;
	for(i=0;i<tMax;i++)
	{
		for(j=0;j<tCand[i]->nombreChoix;j++)
		{
			
			if(strcmp(tCand[i]->tChoix[j]->ville,"Clermont-Ferrand")==0 && strcmp(tCand[i]->tChoix[j]->dep,"Informatique")==0)
			{
				compt++;
			}
		}
	}
	return compt;
}


// ###########################################################################################
// ###########################################################################################
// ################################ Commun au 2 parties ######################################
// ###########################################################################################
// ###########################################################################################

Candidat ** login(Candidat *tCand[],int *tMax,int *pos, int *retour) /* Affiche un menu de connexion pour ensuite adapter l'affichage et les fonctionnalités au type d'utilisateur */
{
	int i = 3,trouve,numC;
	char id, mdp[31] = "mettez20svp", mdpatrouve[31],choix; /* Déclaration d'un motdepasse administrateur */
	system("clear");
	printf("\t################################ Bienvenue! ################################\nSouhaitez-vous vous connecter en tant que candidat, administeur ou responsable d'admissions ? (C/A/R)\nSaisie : ");
	scanf("%c%*c",&id);
	if(id == 'q') /* Si l'utilisateur saisie la lettre 'q', le programme s'arrête */
	{
		*retour = -1;
		return tCand;
	}
	while(id != 'A' && id != 'a' && id != 'C' && id != 'c' && id != 'r' && id != 'R') /* Effectue une saisie contrôlée si la saisie ne correspond à aucune option */
	{
		system("clear");
		printf("\t#################### Mauvaise saisie ('q' pour quitter) ####################\nSouhaitez-vous vous connecter en tant que candidat, administrateur ou responsable d'admissions ? (C/A/R)\nSaisie : ");
		scanf("%c%*c",&id);
		if(id == 'q')
		{
			*retour = -1;
			return tCand;
		}
	}
	if(id == 'A' || id == 'a') /* Si l'utilisateur sélectionne administrateur, il doit ensuit taper le mot de passe */
	{
		while(i != 0)
		{
			printf("\n\nMot de passe : ");
			system("stty -echo"); /* On masque la saisie pour saisir le mot de passe */
			fgets(mdpatrouve, 31, stdin);
			mdpatrouve[strlen(mdpatrouve) - 1] = '\0';
			printf("\n");
			if(strcmp(mdpatrouve, mdp) == 0 ) /* Si le mot de passe est bon, la fonction retourne 1 */
			{
				system("stty echo");
				system("clear");
				*retour = 1;
				return tCand;
			}
			else /* Si le mot de passe est faux, l'utilisateur dispose de 2 essais supplémentaire */
			{
				i--;
				printf("Mot de passe incorrect, il vous reste %d chances\n",i);
			}
			system("stty echo");
		}
		*retour = -1;
		return tCand;
	}
	if(id == 'C' || id == 'c')/* Si l'utilisateur sélectionne candidat, la fonction retourne 0 */
	{
		printf("Êtes-vous déjà enregistré / Avez-vous un numéro de candidat ?(o/n)\t");
		scanf("%c%*c",&choix);
		while(choix!='o'&&choix!='O'&&choix!='n'&&choix!='N')
		{
			printf("Êtes-vous déjà enregistré / Avez-vous un numéro de candidat ?(o/n)\t");
			scanf("%c%*c",&choix);
		}
		if(choix=='o'|| choix=='O')
		{
			printf("\nInsérer votre numéro de Candidat :\t");
			scanf("%d%*c",&numC);
			*pos=rechercherCandidat(tCand,*tMax,numC,&trouve);
			while(trouve==0)
			{
				printf("\nCe numéro n'existe pas. Veuillez Re-saisir :\t");
				scanf("%d%*c",&numC);
				*pos=rechercherCandidat(tCand,*tMax,numC,&trouve);
			}
			*retour = 0;
			return tCand;
		}
		if(choix=='n'|| choix=='N')
		{
			tCand=creerCandidat(tCand,tMax,pos);
			*retour = 0;
			return tCand;
		}
		
	}
	if(id == 'R' || id == 'r')
	{
		*retour = 3;
		return tCand;
	}
}

void menuAdmin(VilleIUT *tiut[], int *tLog, int tMax,Candidat *tCand[],int tMaxC,int *phaseCandidature,FileCand *f,ListeCand *ladmis)/* Affiche un menu de choix adaptés pour un administrateur et appelle les fonctions en conséquence */
{
	int select, pos;
	int trouve,nbClfd;
	char ville[31], choix;
	Candidat **tIntermediaire;
	ListeDept l;
	printf("__________________________________________________________\n");
	printf("|                     MENU ADMIN                         |\n");
	printf("|--------------------------------------------------------|\n");
	printf("| 1 Afficher les villes possédant un IUT                 |\n");
	printf("| 2 Afficher les départements d'un IUT                   |\n");
	printf("| 3 Créer un nouveau département                         |\n");
	printf("| 4 Supprimer un département                             |\n");
	printf("| 5 Effectuer une mise à jour d'un département d'un IUT  |\n");
	printf("| 6 Insérer un nouvel IUT                                |\n");
	printf("| 7 Supprimer un IUT de la liste                         |\n");
	printf("| 8 Lancer/Arrêter la phase de candidature               |\n");
	printf("| 9 Quitter                                              |\n");
	printf("|--------------------------------------------------------|\n\n");
	printf("Saisie : "); /* Choix de l'administrateur */
	scanf("%d%*c",&select);
	while(select != 9) /* Tant que l'utilisateur ne saisie pas 9, le menu réapparait après que la fonction appelée est terminée */
	{
		system("clear");
		if(select == 1)
		{
			afficherTIUT(tiut, *tLog);
		}
		if(select == 2)
		{
			printf("Pour quelle ville souhaitez-vous afficher les départements ?\nSaisie : "); /* Demande une ville à afficher */
			scanf("%s%*c", ville);
			printf("\n");
			pos = rechercheIUT(tiut, *tLog, ville, &trouve); /* Vérifie que la ville existe */
			while(trouve == 0) /* Si elle n'existe pas, on effectue une saisie contrôlée */
			{
				printf("Cette ville n'existe pas, veuillez re-sasisir : ");
				scanf("%s%*c", ville);
				pos = rechercheIUT(tiut, *tLog, ville , &trouve);
				printf("\n");
			}
			afficherVilleDep(*tiut[pos]); /* Affiche ensuite la ville */
		}
		if(select == 3)
		{
			creerDept(tiut, *tLog);
		}
		if(*phaseCandidature==0)
		{
				if(select == 4)
			{
				retirerDept(tiut, *tLog);
			}
			if(select == 5)
			{
				miseAJourGlobale(tiut, *tLog);
			}
			if(select == 6)
			{
				printf("Quel est le nom de la ville à insérer ?\nSaisie : "); /* Demande le nom de la ville à insérer */
				scanf("%s%*c", ville);
				printf("\n");
				pos = rechercheIUT(tiut, *tLog, ville, &trouve); /* Vérifie que la ville existe */
				if(trouve == 1) /* Si elle existe, affiche un message d'erreur */
				{
					printf("Erreur, cette ville existe déjà !\n");
				}
				else /* Sinon, appelle la fonction d'insertion */
				{
					insererVille(tiut, ville, tLog, tMax, pos);
				}
			}
			if(select == 7)
			{
				printf("Quel est le nom de la ville à supprimer ?\nSaisie : "); /* Demande le nom de la ville à supprimer */
				scanf("%s%*c", ville);
				printf("\n");
				pos = rechercheIUT(tiut, *tLog, ville, &trouve); /* Vérifie qu'elle existe */
				if(trouve == 0) /* Si elle n'existe pas, affiche un message d'erreur */
				{
					printf("Erreur, cette ville n'existe pas !\n");
				}
				else /* Sinon, demande confirmation, si confirmé, supprime la ville, sinon annule la suppression */
				{
					printf("Êtes-vous sur de vouloir supprimer cette ville ? (O/n)\nSaisie : "); /* Demande confirmation de la suppression */
					scanf("%c%*c", &choix);
					printf("\n");
					if(choix == 'N' || choix == 'n') /* Si le choix et non, annule la supression */
					{
						printf("Suppression annulée\n");
					}
					else
					{
						supprimerVille(tiut, ville, tLog, pos);
					}
				}
			}
		}
		if(select==8)
		{
			lancerPhaseCandidature(phaseCandidature);
			if(*phaseCandidature==2)
			{
				nbClfd=comptClermont(tCand,tMaxC);
				tIntermediaire=(Candidat **)malloc(nbClfd*sizeof(Candidat*));
				if(tIntermediaire==NULL)
				{
					printf("Problème d'allocation\n");
					exit(1);
				}
				tIntermediaire=remplirCandCl(tCand,tIntermediaire,tMaxC,nbClfd);
				*f=insertFile(tIntermediaire,nbClfd);
			}
		}
		printf("__________________________________________________________\n");
		printf("|                     MENU ADMIN                         |\n");
		printf("|--------------------------------------------------------|\n");
		printf("| 1 Afficher les villes possédant un IUT                 |\n");
		printf("| 2 Afficher les départements d'un IUT                   |\n");
		printf("| 3 Créer un nouveau département                         |\n");
		printf("| 4 Supprimer un département                             |\n");
		printf("| 5 Effectuer une mise à jour d'un département d'un IUT  |\n");
		printf("| 6 Insérer un nouvel IUT                                |\n");
		printf("| 7 Supprimer un IUT de la liste                         |\n");
		printf("| 8 Lancer/Arrêter la phase de candidature               |\n");
		printf("| 9 Quitter                                              |\n");
		printf("|--------------------------------------------------------|\n\n");
		printf("Saisie : ");
		scanf("%d%*c",&select);
	}
}

void menuCandidat(VilleIUT *tiut[], int *tLog, int tMax,Candidat *tCand[],int tMaxC,int posC,int phaseCandidature,FileCand *f,ListeCand *ladmis,float mini)
{
	int select, pos,i,retour,majChoix,j=0;
	char ville[31], dept[31];
	int trouve;
	ListeCand suppr;
	MaillonDept *m;
	system("clear");
	printf("______________________________________________________________\n");
	printf("|                   MENU CANDIDAT                            |\n");
	printf("|------------------------------------------------------------|\n");
    printf("|            Bienvenue %-32s   |\n", tCand[posC]->prenom);
    printf("|------------------------------------------------------------|\n");
    printf("|            Que souhaitez-vous faire ?                      |\n");
	printf("|------------------------------------------------------------|\n");
	printf("| 1 Afficher les villes où il y a un IUT                     |\n");
	printf("| 2 Afficher tous les départements dans un IUT           |\n");
	printf("| 3 Nombres de places en première année                      |\n");
	printf("| 4 Afficher les IUT possédant le département voulu          |\n");
	printf("| 5 Afficher vos choix                                       |\n");
    printf("| 6 Ajouter un choix                                         |\n");
    printf("| 7 Supprimer un choix                                       |\n");
    printf("| 8 Mettre à jour votre décision concernant un établissement |\n");
	printf("| 9 Quitter                                                  |\n");
	printf("|____________________________________________________________|\n");
	printf("Saisie : ");
	scanf("%d%*c",&select);
	while(select != 9)
	{	
		system("clear");
		if(select==1)
		{
			afficherTIUT(tiut,*tLog);
		}
		if(select==2)
		{
			printf("Quel IUT souhaitez-vous chercher \nSaisie : ");
			scanf("%s%*c",ville);
			printf("\n");
			pos=rechercheIUT(tiut,*tLog,ville,&trouve);
			while(trouve == 0)
			{
				printf("Cette ville n'existe pas, veuillez-resaisir\nSaisie : ");
				scanf("%s%*c",ville);
				pos=rechercheIUT(tiut,*tLog,ville,&trouve);
				printf("\n");
			}
			afficherVilleDep(*tiut[pos]);
		}
		if(select==3)
		{
			printf("Quel IUT ?\nSaisie : ");
			scanf("%s%*c",ville);
			pos=rechercheIUT(tiut,*tLog,ville,&trouve);
			while(trouve==0)
			{
				printf("Cette ville n'existe pas, veuillez-resaisir\nSaisie : ");
				scanf("%s%*c",ville);
				pos=rechercheIUT(tiut,*tLog,ville,&trouve);
				printf("\n");
			}
			printf("Quel département ?\nSaisie : ");
			scanf("%s%*c",dept);
			m=rechercherDept(tiut[pos]->lDept,dept,&trouve);
			while(trouve==0)
			{
				printf("Ce département n'existe pas, veuillez-resaisir\nSaisie : ");
				scanf("%s%*c",dept);
				m=rechercherDept(tiut[pos]->lDept,dept,&trouve);
			}
			afficherPlace(m->d);
			clearpage();
		}
		if(select==4)
		{
			afficheDeptDesIUT(tiut,*tLog);
		}
		if(phaseCandidature==1)
		{
			if(select == 5)
			{
				for(i = 0; i < tCand[posC]->nombreChoix; i++)
				{
					afficherChoix(tCand[posC]->tChoix[i]);
				}
			}
			if(select == 6)
			{
				tCand[posC]->tChoix = insererChoix(tCand[posC]->tChoix, &tCand[posC]->nombreChoix);
				for(i = 0; i < tCand[posC]->nombreChoix; i++)
				{
					afficherChoix(tCand[posC]->tChoix[i]);
				}
				clearpage();
			}
			if(select == 7)
			{
				tCand[posC]->tChoix = supprimerChoix(tCand[posC]->tChoix, &tCand[posC]->nombreChoix);
				for(i = 0; i < tCand[posC]->nombreChoix; i++)
				{
					afficherChoix(tCand[posC]->tChoix[i]);
				}
				clearpage();
			}
		}
		if(phaseCandidature==2)
        {
			if(select == 8)
			{
				if (majChoix==1)
				{
					printf("Vous-avez déjà validé votre choix vous ne pouvez plus changer à présent.\n");
					clearpage();
				}
				else
				{
					retour=miseAJourChoixCand(tCand[posC]->tChoix, tCand[posC]->nombreChoix);
					if (retour==-2 || retour==0)
					{
						printf("Vos choix n'ont pas été mis à jour.\n");
						clearpage();
					}
					if (retour==1)
					{
						while(j<tCand[posC]->nombreChoix)
						{
							if (tCand[posC]->tChoix[j]->decisionCand!=1)
							{
								tCand[posC]->tChoix[j]->decisionCand=-1;
								j++;
							}
							
						}
						majChoix=1;
					}
					if(retour==-1)
					{
						while (j<tCand[posC]->nombreChoix)
						{
							if(strcmp(tCand[posC]->tChoix[j]->ville, "Clermont-Ferrand") == 0 && strcmp(tCand[posC]->tChoix[j]->dep, "Informatique") == 0)
							{
								tCand[posC]->tChoix[j]->decisionCand=-1;
								break;
							}
						}
						suppr=rechercherCandListe(*ladmis,tCand[posC]->numeroC,&trouve);
						*ladmis=supprimerCand(*ladmis,tCand[posC]);
						*f=fileToAdmis(*f,ladmis,1,mini);
					}
				}
			}
		}
		printf("______________________________________________________________\n");
		printf("|                   MENU CANDIDAT                            |\n");
		printf("|------------------------------------------------------------|\n");
		printf("|            Bienvenue %-32s   |\n", tCand[posC]->prenom);
		printf("|------------------------------------------------------------|\n");
		printf("|            Que souhaitez-vous faire ?                      |\n");
		printf("|------------------------------------------------------------|\n");
		printf("| 1 Afficher les villes où il y a un IUT                     |\n");
		printf("| 2 Afficher tous les départements dans chaque IUT           |\n");
		printf("| 3 Nombres de places en première année                      |\n");
		printf("| 4 Afficher les IUT possédant le département voulu          |\n");
		printf("| 5 Afficher vos choix                                       |\n");
		printf("| 6 Ajouter un choix                                         |\n");
		printf("| 7 Supprimer un choix                                       |\n");
		printf("| 8 Mettre à jour votre décision concernant un établissement |\n");
		printf("| 9 Quitter                                                  |\n");
		printf("|____________________________________________________________|\n");
		printf("Saisie :");
		scanf("%d%*c",&select);
	}	
}


void menuResp(Candidat *tCand[],int tMax,int phaseCandidature,FileCand *f,ListeCand *ladmis,float *mini)
{
	int select=0;
	while(select!=9)
	{
		system("clear");
		printf("___________________________________________________________\n");
		printf("| Menu Responsable                                        |\n");
		printf("|---------------------------------------------------------|\n");
		printf("| 1 Déclarez une moyenne minimale (obligatoire)           |\n");
		printf("| 2 Gérer les admissions                                  |\n");
		printf("| 3 Afficher la liste des admissions ou la file d'attente |\n");
		printf("| 9 Quitter                                               |\n");
		printf("|_________________________________________________________|\n");
		printf("\nSaisie :\t");
		scanf("%d%*c",&select);
		if(select==1)
		{
			printf("Quelle est la moyenne minimale pour entrer dans votre département?\t");
			scanf("%f%*c",mini);
		}
		if(select==2)
		{
			gererAdmis(f,ladmis,*mini);
		}
		if(select==3)
		{
			afficherCandidatsParCategorie(*f,*ladmis);
		}
	}
	clearpage();
}

void gererAdmis(FileCand *f, ListeCand *l,float mini)
{
	int select=0,admis,i=0;
	char choix;
	Candidat *c;
	while (select!=9)
	{
		system("clear");
		printf("|--------------------------|\n");
		printf("| 1 Accepter des candidats |\n");
		printf("| 2 Refuser des candidats  |\n");
		printf("| 9 Quitter                |\n");
		printf("|--------------------------|\n");
		printf("Saisie : ");
		scanf("%d%*c",&select);
		if(select==1)
		{
			afficher(*f);
			printf("\nCombien de candidats voulez vous accepter?\t");
			scanf("%d%*c",&admis);
			if(admis == 0)
			{
				printf("Vous ne pouvez pas accepter 0 candidats\n");
				continue;
			}
			*f=fileToAdmis(*f,l,admis,mini);
			afficherListeCand(*l);
		}
		if(select==2)
		{
			c=tete(*f);
			afficherCandidat(c);
			printf("Refuser le candidat?(o/n)\nSaisie : ");
			scanf("%c%*c",&choix);
			printf("\n");
			if(choix=='o'||choix=='O')
			{
				*f=supt(*f);
				while(i<c->nombreChoix)
				{
					if (strcmp(c->tChoix[i]->ville, "Clermont-Ferrand") == 0 && strcmp(c->tChoix[i]->dep, "Informatique") == 0)
					{
						c->tChoix[i]->decisionResp=-1;
					}
					i++;
				}
			}
		}

	}
	clearpage();
}

FileCand fileToAdmis(FileCand f, ListeCand *l, int nbAdmis,float mini)
{
	int j;
	Candidat *c;
	while(nbAdmis != 0)
	{
		if(f.t->c->moyenne >= mini)
		{
			c=f.t->c;
			f=supt(f);
			*l=insererCandEntete(*l,c);
			for(j = 0; j < c->nombreChoix; j++)
			{
				if(strcmp(c->tChoix[j]->ville, "Clermont-Ferrand") == 0 && strcmp(c->tChoix[j]->dep, "Informatique") == 0)
				{
					c->tChoix[j]->decisionResp = 1;
					break;
				}
			}
			nbAdmis = nbAdmis - 1;
		}
		else 
		{
			f=supt(f);
			for(j = 0; j < c->nombreChoix; j++)
			{
				if(strcmp(c->tChoix[j]->ville, "Clermont-Ferrand") == 0 && strcmp(c->tChoix[j]->dep, "Informatique") == 0)
				{
					c->tChoix[j]->decisionResp = -2;
					break;
				}
			}
		}
	}
	clearpage();
	return f;
}


FileCand insertFile(Candidat *tCand[],int nbClfd)
{
	int i, j;
	FileCand f;
	f=filenouv();
	for(i=0;i<nbClfd;i++)
	{
		if(tCand[i]->moyenne >= 12.0)
		{
			f=adjq(f,tCand[i]);
			for(j = 0; j < tCand[i]->nombreChoix; j++)
			{
				if(strcmp(tCand[i]->tChoix[j]->ville, "Clermont-Ferrand") == 0 && strcmp(tCand[i]->tChoix[j]->dep, "Informatique") == 0)
				{
					tCand[i]->tChoix[j]->decisionResp = 2;
					break;
				}
			}
		}
		else
		{
			if(strcmp(tCand[i]->tChoix[j]->ville, "Clermont-Ferrand") == 0 && strcmp(tCand[i]->tChoix[j]->dep, "Informatique") == 0)
				{
					tCand[i]->tChoix[j]->decisionResp = -1;
				}
		}
	}
	return f;
}



void clearpage(void) /* Permet de demander à l'utilisateur pour continuer à la suite d'une action et efface le contenu affiché à l'écran */
{
    char entre;
    printf("\nAppuyez sur la touche [ENTREE] pour continuer");
    scanf("%c", &entre);
    system("clear");
}

void globale(void) /* Permet l'initialisation du tableau, le chargement de celui-ci et l'appel des fonctions login, menus et enregistrement */
{
	int tLog, retour,tMax,numC,phaseCandidature=0,pos;
	float mini;
	Candidat **tCand;
	VilleIUT *tiut[100]; /* Déclaration du tableau de pointeurs tiut */
	FileCand f;
	ListeCand  ladmis;
	ladmis = listeCandNouv();
	if(tiut == NULL) /* En cas de problème de mémoire, affiche un message d'erreur et quitte le programme */
	{
		printf("Problème d'allocation mémoire du tableau tiut\n");
		exit(1);
	}
	tLog = chargement(tiut,100); /* Effectue le chargement du tableau tiut */
	if(tLog < 0) /* En cas de problème avec le chargement, affiche un message d'erreur et quitte le programme */
	{
		printf("Le programme ne peut pas fonctionner\n");
		exit(1);
	}
	// Chargement des candidats
	tCand = chargementCandidats(&tMax);
	triCandidats(tCand, tMax);
	tCand = login(tCand,&tMax,&pos, &retour); /* Appelle la fonction login */
	while(retour != -1) /* Tant que login ne retourne pas -1, la fonction appelle le menu adaptée en fonction du retour de login puis appelle de nouveau login */
	{
		if(retour == 1)
		{
			menuAdmin(tiut, &tLog, 100,tCand,tMax,&phaseCandidature,&f,&ladmis);
		}
		if(retour == 0)
		{
			menuCandidat(tiut, &tLog, 100,tCand,tMax,pos,phaseCandidature,&f,&ladmis,mini);
		}
		tCand = login(tCand,&tMax,&pos, &retour);
		if(retour==3)
		{
			menuResp(tCand,tMax,phaseCandidature,&f,&ladmis,&mini);
		}
	}
	enregistrement(tiut, tLog);
	sauvegarder(tCand, tMax);
}





void enregistrement(VilleIUT *tiut[],int tLog) /* Permet l'enregistrement du tableau tiut dans le fichier IUT.don */
{
	int i = 0;
	FILE *flot;
	flot = fopen("IUT.don", "w"); /* Ouverture du fichier IUT.don dans la variable flot */
	if(flot == NULL) /* En cas d'erreur de l'ouverture du fichier, affiche un message d'erreur, ferme le fichier et quitte la fonction */
	{
		printf("Erreur de l'enregistrement du fichier\n");
		fclose(flot);
		return;
	}
	while(i < tLog) /* Boucle allant jusqu'à la dernière ville du tableau */
	{
		while(tiut[i]->lDept != NULL) /* Boucle allant jusqu'au dernier département de la ville */
		{
			fprintf(flot, "%s\t%s\t%d\t%s\n", tiut[i]->nom, tiut[i]->lDept->d.dept, tiut[i]->lDept->d.nbP, tiut[i]->lDept->d.respAd); /* Écriture dans le fichier */
			tiut[i]->lDept = tiut[i]->lDept->suiv;
		}
		i = i + 1; /* Incrémentation de l'indice i pour passer à la ville suivante */
	}
	fclose(flot);
}


// ###########################################################################################
// ###########################################################################################
// ################################ Partie II ################################################
// ###########################################################################################
// ###########################################################################################

/************************************** Fonctions de Chargement ********************************************/
/****************************************** Et de sauvegarde*************************************************/
/************************************************************************************************************/

Candidat ** chargementCandidats(int *tMax) /* Permet de charger le contenu du fichier Candidats.don dans un tableau de pointeur vers */
{                                          /* des structures Candidats qui contiennent un tableau de pointeurs vers des structures choix*/
    FILE *flot;                            /* La fonction renvoie ensuite le tableau de Candidats si tout se passe bien */
    Candidat **tCand, *c; /* Déclaration du tableau de candidats et d'une variable de chargement */
    int i;
    flot = fopen("Candidats.don", "r");
    if(flot == NULL)
    {
        printf("Erreur lors de l'ouverture du fichier candidat\n");
        fclose(flot);
        exit(1);
    }
    fscanf(flot, "%d", tMax); /* Lecture du nombre de candidats pour déterminer la taille physique du tableau */
    tCand = (Candidat **)malloc(*tMax * sizeof(Candidat *)); /* Allocation dynamique du tableau de candidats*/
    if(tCand == NULL)
    {
        printf("Erreur d'allocation mémoire tableau candidat\n");
        fclose(flot);
        exit(1);
    }
    for(i = 0; i < *tMax; i++) /* Remplissage du tableau */
    {
        c = lireCandidat(flot); /* Lecture d'un candidat */
        tCand[i] = c; /* Insertion du candidat lu dans le tableau */
    }
    fclose(flot);
	afficherCandChoix(tCand, *tMax);
    return tCand;
}

Candidat * lireCandidat(FILE *flot) /* Fonction permettant de lire un candidat dans un fichier et renvoyant un pointeur vers la structure candidat lue */
{
    Candidat *c; /* Déclaration d'un pointeur vers un candidat pour la lecture et l'allocation dynamique d'un candidat */
    Choix choix; /* Déclaration d'une variable de type Choix pour la lecture des choix du candidat */
    int i = 0;
    c = (Candidat *)malloc(sizeof(Candidat)); /* Allocation dynamique du candidat */
    if(c == NULL)
    {
        printf("Erreur d'allocation mémoire candidat\n");
        fclose(flot);
        exit(1);
    }
    fscanf(flot, "%d%*c", &c->numeroC); /* Lectures des informations du candidat (numero, nom, prenom, notes, nombre de choix)*/
    fgets(c->nom, 31, flot);
    c->nom[strlen(c->nom) - 1] = '\0';
    fgets(c->prenom, 31, flot);
    c->prenom[strlen(c->prenom) - 1] = '\0';
    fscanf(flot, "%f%f%f%f", &c->notes[0], &c->notes[1], &c->notes[2], &c->notes[3]);
    fscanf(flot, "%d%*c", &c->nombreChoix);
	c->moyenne = (c->notes[0] + c->notes[1] + c->notes[2] + c->notes[3]) / 4;
    c->tChoix = (Choix **)malloc(sizeof(Choix *) * c->nombreChoix); /* Allocation dynamique du tableau de pointeurs vers des structures Choix*/
    if(c->tChoix == NULL)                                           /* pour le chargement des choix du candidats */
    {
        printf("Erreur lors de l'allocation mémoire du tableau de choix\n");
        fclose(flot);
        exit(1);
    }
    while(i < c->nombreChoix) /* Lecture des choix du candidats et placements de ceux-ci dans le tableau tChoix */
    {
        c->tChoix[i] = lireChoix(flot);
        i = i + 1;
    }
    return c;
}

Choix * lireChoix(FILE *flot) /* Fonction permettant de lire le Choix d'un candidat dans un fichier et renvoyant un pointeur vers la structure choix*/
{                             /* Du choix qui a été lu */
    Choix *c; /* Déclaration d'un pointeur vers une variable de type choix pour la lecture */
    c = (Choix *)malloc(sizeof(Choix)); /* Allocation dynamique de la structure choix */
    if(c == NULL)
    {
        printf("Erreur d'allocation mémoire choix\n");
        exit(1);
    }
    fgets(c->ville, 31, flot); /* Lectures des différentes informations relatives au choix */
    c->ville[strlen(c->ville) - 1] = '\0';
    fgets(c->dep, 31, flot);
    c->dep[strlen(c->dep) - 1] = '\0';
    fscanf(flot, "%d%d%*c", &c->decisionResp, &c->decisionCand);
    return c;
}

void sauvegarder(Candidat *tCand[], int tMax) /* Fonctions permettant de sauvegarder l'ensemble des candidats et de leur choix dans un fichier */
{                                             /* CandidatsSauv.don */
    FILE *flot;
    int i, j;
    flot = fopen("Candidats.don", "w"); /* Ouverture du fichier */
    if(flot == NULL)
    {
        printf("Erreur de l'ouverture du fichier lors de l'enregistrement\n");
        fclose(flot);
        exit(1);
    }
    fprintf(flot, "%d\n", tMax); /* Ecriture su nombre de candidats */
    for(i = 0; i < tMax; i++) /* Ecriture de chaque candidat*/
    {
        fprintf(flot, "%d\n%s\n%s\n%.2f\t%.2f\t%.2f\t%.2f\n%d\n", tCand[i]->numeroC, tCand[i]->nom, tCand[i]->prenom, tCand[i]->notes[0], tCand[i]->notes[1], tCand[i]->notes[2], tCand[i]->notes[3], tCand[i]->nombreChoix);
        for(j = 0; j < tCand[i]->nombreChoix; j++) /* Ecriture de chaque choix du candidat */
        {
            fprintf(flot, "%s\n%s\n%d\n%d\n", tCand[i]->tChoix[j]->ville, tCand[i]->tChoix[j]->dep, tCand[i]->tChoix[j]->decisionResp, tCand[i]->tChoix[j]->decisionCand);
        }
    }
    fclose(flot);
    return;
}


/***************************************** Fonctions d'affichage ********************************************/
/************************************************************************************************************/
/************************************************************************************************************/

void afficherChoix(Choix *c) /* Fonction permettant d'afficher les informations d'un choix donné en paramètre */
{
    printf("|_______________________________________________________________________________|\n");
    printf("| %-32s | %-32s | %2d | %2d |\n", c->ville, c->dep, c->decisionResp, c->decisionCand);
    printf("|-------------------------------------------------------------------------------|\n");
}

void afficherCandidat(Candidat *c) /* Fonction permettant d'afficher les informations d'un candidat donné en paramètre */
{
    printf("|-------------------------------------------------------------------------------------------------------------------|\n");
    printf("| %-4d | %-32s | %-32s | %2.2f | %2.2f | %2.2f | %2.2f | %2.2f |\n", c->numeroC, c->nom, c->prenom, c->notes[0], c->notes[1], c->notes[2], c->notes[3], c->moyenne);
    printf("|-------------------------------------------------------------------------------------------------------------------|\n");
}

void afficherCandChoix(Candidat *tCand[],int tMax)  /* Fonction permettant d'afficher tous les candidats du tableau tCand ainsi que tous leurs choix */
{
    int i, j;
    for(i = 0; i < tMax; i++)
    {
        printf("_______________________________________________________________________________________________________________________\n");
        printf("|                                                        Candidat                                                     |\n");
        afficherCandidat(tCand[i]);
        printf("\n");
        printf("_______________________________________________________________________________\n");
        printf("|                                  Choix                                      |\n");
        for(j = 0; j < tCand[i]->nombreChoix; j++)
        {
            afficherChoix(tCand[i]->tChoix[j]);
        }
    }
}

void afficherCandDep(Candidat *tCand[], int tMax) /* Fonction permettant d'afficher tous les candidats ayant fait une demande pour un département */
{
    int i, j;
    char ville[31], dep[31];
    printf("Dans quelle ville se trouve le départment dont vous souhaitez afficher les candidats ?\nSaisie : ");
    scanf("%s%*c", ville);
    printf("\n");
    printf("Quelle est le nom du département dont vous souhaitez afficher les candidats ?\nSaisie : ");
    scanf("%s%*c", dep);
    printf("\n");
    for(i = 0; i < tMax; i++)
    {
        for(j = 0; j < tCand[i]->nombreChoix; j++)
        {
            if(strcmp(tCand[i]->tChoix[j]->ville, ville) == 0 && strcmp(tCand[i]->tChoix[j]->dep, dep) == 0)
            {
                afficherCandidat(tCand[i]);
            }
        }
    }
}


/************************************ Fonctions de réallocation *********************************************/
/************************************************************************************************************/
/************************************************************************************************************/

Candidat ** reallocationCand(Candidat *tCand[], int tMax) /* Fonction permettant de re-allouer le tableau des candidats en ajoutant 1 à sa taile physique */
{
    Candidat **aux;
    aux = (Candidat **)realloc(tCand, sizeof(Candidat *) * (tMax + 1));
    if(aux == NULL)
    {
        printf("Erreur lors de la réallocation du tableau\n");
		exit(1);
    }
    return aux;
}

Choix ** reallocationChoix(Choix *tChoix[], int nbChoix) /* Fonction permettant de re-allouer un tableau de choix en ajoutant 1 à sa taille physique */
{
    Choix **aux;
    nbChoix = nbChoix + 1;
    aux = (Choix **)realloc(tChoix, nbChoix * sizeof(Choix *));
    if(aux == NULL)
    {
        printf("Erreur lors de la réallocation du tableau\n");
		exit(1);
    }
    return aux;
}


/************************************* Fonctions de tri *****************************************************/
/************************************************************************************************************/
/************************************************************************************************************/

void triCandidats(Candidat *tCand[], int tMax) /* Fonction permettant de trier un tableau de candidats en effectuant un tri par échange */
{
    int maxi;
    while(tMax > 1)
    {
        maxi = plusGrandCand(tCand, tMax); /* Appel de la fonction plusGrand pour déterminer le plus grand élément du tableau */
        echangerCand(tCand, maxi, tMax - 1); /* Appel de la fonction échange pour placer le plus grnad élément du tableau en dernière position */
        tMax = tMax - 1;
    }
}

int plusGrandCand(Candidat *tCand[], int tMax) /* Fonction permettant de trouver le plus grand élément du tableau candidat en le parcourant et en*/
{                                              /* comparant ses éléments. Elle renvoie ensuite l'indice du plus grand élément */
    int maxi = 0, i;
    for(i = 1; i < tMax; i++)
    {
        if(strcmp(tCand[i]->nom, tCand[maxi]->nom) > 0)
        {
            maxi = i;
        }
    }
    return maxi;
}

void echangerCand(Candidat *tCand[], int i, int j) /* Fonction permettant d'échanger deux éléments dans un tableau de candidats*/
{
    Candidat *aux;
    aux = tCand[i];
    tCand[i] = tCand[j];
    tCand[j] = aux;
}

void triChoix(Choix *tChoix[], int nombreChoix) /* Fonction de tri par échange permettant de trier un tableau de Choix */
{
    int maxi;
    while(nombreChoix > 1)
    {
        maxi = plusGrandChoix(tChoix, nombreChoix);
        echangerChoix(tChoix, maxi, nombreChoix - 1);
        nombreChoix = nombreChoix - 1;
    }
}

int plusGrandChoix(Choix *tChoix[], int nombreChoix) /* Fontcion permettant de trouver le plus grand élément d'un tableau de choix et de renvoyer son indice */
{
    int maxi = 0, i;
    for(i = 1; i < nombreChoix; i++)
    {
        if(strcmp(tChoix[i]->ville, tChoix[maxi]->ville) == 0)
        {
            if(strcmp(tChoix[i]->dep, tChoix[maxi]->dep) > 0)
            {
                maxi = i;
            }
        }
        if(strcmp(tChoix[i]->ville, tChoix[maxi]->ville) > 0)
        {
            maxi = i;
        }
    }
    return maxi;
}

void echangerChoix(Choix *tChoix[], int i, int j) /* Fonction permettant d'échanger deux éléments d'un tableau de choix */
{
    Choix *aux;
    aux = tChoix[i];
    tChoix[i] = tChoix[j];
    tChoix[j] = aux;
}


/******************************* Fonctions d'insertion/suppresion/maj ***************************************/
/************************************************************************************************************/
/************************************************************************************************************/

Candidat ** creerCandidat(Candidat *tCand[], int *tMax,int *pos)
{
    int trouve, i, numeroC;
    char nom[31], prenom[31];
    float notes[4];
    Candidat *c;
    printf("Quel est votre numéro de candidat ?\nSaisie : "); /* Demande à l'utilisateur le numéro du candidat pour l'insertion */
    scanf("%d%*c", &numeroC);
    printf("\n");
    *pos = rechercherCandidat(tCand, *tMax, numeroC, &trouve); /* Vérifie que le numero de candidat n'existe pas déjà */
    if(trouve == 1)
    {
        printf("Erreur, votre numéro de candidat existe déjà dans la base de données\n");
        return tCand;
    }
    c = (Candidat *)malloc(sizeof(Candidat)); /* Allocation dynamique du candidat à insérer */
    if(c == NULL)
    {
        printf("Erreur d'allocation mémoire lors de l'insertion du candidat\n");
        return tCand;
    }
    printf("Quel est votre nom ?\n");
    scanf("%s%*c", nom);
    printf("\n");
    printf("Quel est votre prenom ?\n");
    scanf("%s%*c", prenom);
    printf("\n");
    printf("Quel est votre moyenne de maths ?\n");
    scanf("%f%*c", &notes[0]);
    printf("\n");
    printf("Quel est votre moyenne de français ?\n");
    scanf("%f%*c", &notes[1]);
    printf("\n");
    printf("Quel est votre moyenne d'anglais ?\n");
    scanf("%f%*c", &notes[2]);
    printf("\n");
    printf("Quel est votre moyenne de spécialité ?\n");
    scanf("%f%*c", &notes[3]);
    printf("\n");
    c->numeroC = numeroC;
    strcpy(c->nom, nom); /* Ajout des valeurs dans le candidat à insérer */
    strcpy(c->prenom, prenom);
    c->notes[0] = notes[0]; 
    c->notes[1] = notes[1];
    c->notes[2] = notes[2];
    c->notes[3] = notes[3];
	c->nombreChoix = 0;
	c->moyenne = (c->notes[0] + c->notes[1] + c->notes[2] + c->notes[3]) / 4;
    tCand = reallocationCand(tCand, *tMax); /* Réallocation du tableau de candidats pour ajouter une place */
    for(i = *tMax; i > *pos; i--) /* Décalage à droite des éléments du tableau à partir de la position d'insertion */
    {
        tCand[i] = tCand[i - 1];
    }
    tCand[*pos] = c; /* Ajout du candidat dans le tableau */
    *tMax = *tMax + 1; /* Mise à jour de la taille physique du tableau */
    return tCand;
}

Choix ** insererChoix(Choix *tChoix[], int *nombreChoix) /* Fonction permettant d'ajouter un nouveau choix dans un tableau de choix et retournant le tableau*/
{
    int pos, trouve, i;
    char ville[31], dep[31];
    Choix *c;
    printf("Dans quelle ville se trouve la formation que vous souhaitez ajouter à vos choix ?\nSaisie : "); /* Demande à l'utilisateur la ville dans laquelle insérer */
    scanf("%s%*c", ville);
    printf("\n");
    printf("Quelle est le nom de la formation que vous souhaitez ajouter à vos choix ?\nSaisie : "); /* Demande le département dans la ville */
    scanf("%s%*c", dep);
    printf("\n");
    pos = rechercherChoix(tChoix, *nombreChoix, ville, dep, &trouve); /* Vérifie que le choix ne figure pas déjà dans le tableau de choix */
    if(trouve == 1)
    {
        printf("Erreur, ce choix figure déjà dans votre liste\n");
        return tChoix;
    }
    c = (Choix *)malloc(sizeof(Choix)); /* Allocation dynamique du choix à insérer */
    if(c == NULL)
    {
        printf("Erreur d'allocation mémoire lors de l'insertion du choix\n");
        return tChoix;
    }
    strcpy(c->ville, ville); /* Ajout des valeurs dans le choix à insérer */
    strcpy(c->dep, dep);
    c->decisionResp = 0; /* Par défaut, les décisions du candidat et du responsable sont initialisées à 0*/
    c->decisionCand = 0;
    tChoix = reallocationChoix(tChoix, *nombreChoix); /* Réallocation du tableau de choix pour ajouter une place */
    for(i = *nombreChoix; i > pos; i--) /* Décalage à droite des éléments du tableau à partir de la position d'insertion */
    {
        tChoix[i] = tChoix[i - 1];
    }
    tChoix[pos] = c; /* Ajout du choix dans le tableau */
    *nombreChoix = *nombreChoix + 1; /* Mise à jour de la taille physique du tableau */
    return tChoix;
}

Choix ** supprimerChoix(Choix *tChoix[], int *nombreChoix) /* Fonction permettant de supprimer un choix d'un tableau de choix */
{
    Choix **aux; /* Déclaration d'une variable aux prenant la valeur du tableau */
    int i, pos, trouve;
    char ville[31], dep[31], saisie;
    printf("Dans quelle ville se trouve la formation que vous souhaitez retirer de vos choix ?\nSaisie : "); /* Demande au candidat dans quelle ville supprimer le choix */
    scanf("%s%*c", ville);
    printf("\n");
    printf("Quelle est le nom de la formation que vous souhaitez retirer de vos choix ?\nSaisie : "); /* Demande au candidat dans quel département de la ville */
    scanf("%s%*c", dep);
    printf("\n");
    pos = rechercherChoix(tChoix, *nombreChoix, ville, dep, &trouve); /* Vérification que le choix indiqué figure bien dan le tableau des choix */
    if(trouve == 0)
    {
        printf("Erreur, ce choix ne figure pas dans votre liste\n");
        return tChoix;
    }
    printf("Êtes-vous sur de vouloir supprimer ce choix ? (O/n)\nSaisie : "); /* Demande de confirmation de la supression */
    scanf("%c%*c", &saisie);
    printf("\n");
    if(saisie == 'n' || saisie == 'N') /* Si non, retourne le tableau sans le changer */
    {
        return tChoix;
    }
    for(i = pos; i < *nombreChoix - 1; i++) /* Décalage à gauche des éléments du tableau à partir du dernier jusqu'au choix à supprimer */
    {
        tChoix[i] = tChoix[i + 1];
    }
    *nombreChoix = *nombreChoix - 1; /* Mise à jour de la taille physique du tableau */
    aux = (Choix **)realloc(tChoix, *nombreChoix * sizeof(Choix *)); /* Réallocation du tableau en enlevant une place à la fin */
    if(aux == NULL)
    {
        printf("Problème avec la réallocation lors de la suppression\n");
        return tChoix;
    }
    return aux;
}


int miseAJourChoixCand(Choix *tChoix[], int nombreChoix) /* Fontcion permettant de mettre à jour la décision d'un candidat concernant un de ses choix */
{
    int pos, trouve, saisie;
    char ville[31], dep[31];
    printf("Dans quelle ville se trouve la formation que vous souhaitez retirer de vos choix ?\nSaisie : "); /* Demande au candidat la ville concernée par le choix */
    scanf("%s%*c", ville);
    printf("\n");
    printf("Quelle est le nom de la formation que vous souhaitez retirer de vos choix ?\nSaisie : "); /* Demande le département dans la ville en question */
    scanf("%s%*c", dep);
    printf("\n");
    pos = rechercherChoix(tChoix, nombreChoix, ville, dep, &trouve); /* Vérifie que ce choix existe bien dans le tableau choix du candidat */
    if(trouve == 0)
    {
        printf("Erreur, ce choix ne figure pas dans votre liste\n");
        return -2; // retour d'erreur
    }
    system("clear");
    if(tChoix[pos]->decisionCand == 0)
    {
        printf("Vous n'avez actuellement pris aucune décision\n\n");
        printf("|---------------------------------------|\n");
        printf("|       Que souhaitez-vous faire ?      |\n");
        printf("|---------------------------------------|\n");
        printf("| 1 Accepter la proposition d'admission |\n");
        printf("| 2 Refuser la proposition d'admission  |\n");
        printf("| 3 Ne rien changer                     |\n");
        printf("|---------------------------------------|\n");
        printf("Saisie : ");
        scanf("%d%*c", &saisie);
        printf("\n");
        if(saisie == 1)
        {
            tChoix[pos]->decisionCand = 1;
            return 1; // choix accepté
        }
        if(saisie == 2)
        {
            tChoix[pos]->decisionCand = -1;
            return -1; // choix refusé
        }
        if(saisie == 3)
        {
            return 0; // n'a rien fait
        }
    }
}

/*********************************** Fonctions de recherche *************************************************/
/************************************************************************************************************/
/************************************************************************************************************/

int rechercherChoix(Choix *tChoix[], int nombreChoix, char ville[], char dep[], int *trouve) /* Permet d'effectuer une recherche dichotomique dans un tableau*/
{                                                                                            /* de choix en comparant les villes et départements */
    int inf = 0, sup = nombreChoix - 1, m;                                                   /* Si le choix et trouvé, la fonction donne la valeur 1 à la*/
    while(inf <= sup)                                                                        /* variable trouve passée par pointeur et renvoie sa position*/
    {                                                                                        /* Si il n'est pas trouvé, la fonction renvoie sa position */
        m = (inf + sup) / 2;                                                                 /* d'insertion et met trouve à 0 */
        if(strcmp(ville, tChoix[m]->ville) == 0 && strcmp(dep, tChoix[m]->dep) == 0)
        {
            *trouve = 1;
            return m;
        }
        if(strcmp(ville, tChoix[m]->ville) > 0)
        {
            inf = m + 1;
        }
        if(strcmp(ville, tChoix[m]->ville) < 0)
        {
            sup = m - 1;
        }
        if(strcmp(ville, tChoix[m]->ville) == 0 && strcmp(dep, tChoix[m]->dep) > 0)
        {
            inf = m + 1;
        }
        if(strcmp(ville, tChoix[m]->ville) == 0 && strcmp(dep, tChoix[m]->dep) < 0)
        {
            sup = m - 1;
        }
    }
    *trouve = 0;
    return inf;
}


int rechercherCandidat(Candidat *tCand[], int tMax, int numeroC, int *trouve) /* Permet d'effectuer une recherche dichotomique dans un tableau de candidats*/
{                                                                             /* en comparants les numéros de candidats. Même principe que pour la fonction*/
    int inf = 0, sup = tMax - 1, m;                                           /* de recherche de choix */
    while(inf <= sup)
    {
        m = (inf + sup) / 2;
        if(numeroC == tCand[m]->numeroC)
        {
            *trouve = 1;
            return m;
        }
        if(numeroC > tCand[m]->numeroC)
        {
            inf = m + 1;
        }
        else
        {
            sup = m - 1;
        }
        
    }
    *trouve = 0;
    return inf;
}

/**************************************** File d'attente ****************************************************/
/************************************************************************************************************/
/************************************************************************************************************/

FileCand filenouv(void)
{
    FileCand f;
    f.t = NULL;
    f.q = NULL;
    return f;
}

FileCand adjq(FileCand f, Candidat *c)
{
    MaillonCandidat *m;
    m = (MaillonCandidat *)malloc(sizeof(MaillonCandidat));
    if(m == NULL)
    {
        printf("Erreur de malloc pour la file d'attente\n");
        exit(1);
    }
    m->c = c;
    m->suiv = NULL;
    if(videFile(f))
    {
        f.t = m;
        f.q = m;
        return f;
    }
    f.q->suiv = m;
    f.q = m;
    return f;
}

FileCand supt(FileCand f)
{
    MaillonCandidat *aux;
    if(videFile(f))
    {
        printf("Opération interdite\n");
        exit(1);
    }
    if(f.t == f.q)
    {
        free(f.t);
        return filenouv();
    }
    aux = f.t;
    f.t = f.t->suiv;
    free(aux);
    return f;
}

Candidat * tete(FileCand f)
{
    if(videFile(f))
    {
        printf("File d'attente vide\n");
        exit(1);
    }
    return f.t->c;
}

int longueurFile(FileCand f)
{
    int i = 0;
    while(f.t != NULL)
    {
        f.t = f.t->suiv;
        i++;
    }
    return i;
}

int positionFileAttente(FileCand f, int numeroC)
{
    int i = 0;
    while(f.t != NULL)
    {
        if(numeroC == f.t->c->numeroC)
        {
            return i;
        }
        f.t = f.t->suiv;
        i++;
    }
    return i;
}

void afficher(FileCand f)
{
    printf("|----------------------------------------------------------------------------|\n");
    printf("|                             Liste d'attente                                |\n");
    printf("|----------------------------------------------------------------------------|\n");
    while(f.t != NULL)
    {
        printf("| %-4d | %-32s | %-32s |\n",f.t->c->numeroC, f.t->c->nom, f.t->c->prenom);
        printf("|----------------------------------------------------------------------------|\n");
        f.t = f.t->suiv;
    }
    printf("\n");
}

bool videFile(FileCand f)
{
	if(f.t == NULL && f.q == NULL)
	{
		return true;
	}
	return false;
}

/************************************* Fonctions de liste d'admission ***************************************/
/************************************************************************************************************/
/************************************************************************************************************/

ListeCand listeCandNouv(void) /*Permet de créer un liste vide puis la retourne à la fonction appelante.*/
{
    ListeCand lCand;
    lCand = NULL;
    return lCand;
}

ListeCand insererCandEntete(ListeCand lCand,Candidat *c) /*Permet d'insérer un MaillonCand en début d'une liste passée en paramètre puis renvoie cette même liste*/
{
    MaillonCandidat *m; /* Création d'un pointeur vers une structure MaillonCand */
    m = (MaillonCandidat *)malloc(sizeof(MaillonCandidat)); /* Allocation d'un espace mémoire pour le nouveau maillon */
    if(m == NULL) 
    {
        printf("Problème d'allocation mémoire lors de l'insertion d'un candidat dans la liste d'admission\n"); /* Message d'erreur en cas de problème de malloc */
        exit(1);
    }
    m->c = c; /* Affecte le département passé en paramètre à l'attribut d du nouveau maillon */
    m->suiv = lCand;
    return m;
}

ListeCand insererCand(ListeCand lCand, Candidat *c) /* Permet d'insérer un maillon dans une liste donnée en paramètre dans l'ordre alphabétique/croissant et retourne cette liste */
{
    if(lCand == NULL) /* Si la liste est vide, insère le nouveau maillon en tête */
    {
        return insererCandEntete(lCand, c);
    }
    if(c->numeroC < lCand->c->numeroC) /* Si le numéro de candidat est inférieur à celui de la liste testé, le maillon est inséré en tête*/
    {
        return insererCandEntete(lCand, c);
    }
    if(c->numeroC == lCand->c->numeroC) /* Si le maillon existe déjà, retourne la liste sans changement */
    {
        printf("Candidat déjà présent dans la liste\n");
        return lCand;
    }
    lCand->suiv = insererCand(lCand->suiv, c); /* Si aucun cas précédent n'est respecté, recommence avec le maillon suivant de la liste */
    return lCand;
}

ListeCand supprimerCandEntete(ListeCand lCand) /* Permet de supprimer un maillon en tête de la liste donnée en paramètre et retourne cette liste */
{
    ListeCand aux;
    if(lCand == NULL) /* Si la liste est vide, quitte le programme car cela provoquerait une erreur */
    {
        printf("Opération interdite\n");
        exit(1);
    }
    aux = lCand; /* On affecte l'adresse de la liste actuelle à une variable temporaire */
    lCand = lCand->suiv; /* On supprime le maillon */
    free(aux); /* On libère l'espace mémoire du maillon supprimer*/
    return lCand;
}

ListeCand supprimerCand(ListeCand lCand, Candidat *c) /* Permet de supprimer un maillon d'une liste lDept passée en paramètre à partir de son attribut nom de département (dept) et retourne cette liste */
{
    if(lCand == NULL) /* La liste est vide, on la retourne sans changements */
    {
        return lCand;
    }
    if(c->numeroC < lCand->c->numeroC) /* Le maillon à supprimer n'existe pas, on retourne la liste sans changement */
    {
        return lCand;
    }
    if(c->numeroC == lCand->c->numeroC) /* Le maillon à supprimer est trouvé, on le supprime */
    {
        return supprimerCandEntete(lCand);
    }
    lCand->suiv = supprimerCand(lCand->suiv, c); /* Aucune des conditions précédentes n'a été respectée, on recommence avec le maillon suivant */
    return lCand;
}

int longueurListe(ListeCand lCand) /* Permet d'obtenir la longueur d'une liste passée en paramètre et retourne le nombre de maillons */
{
    int compt = 0; /* On déclare un compteur pour compter le nombre de maillons */
    while(lCand != NULL) /* Tant que la liste n'est pas vide, on incrémente le compteur de 1 et on passe au maillon suivant */
    {
        compt = compt + 1;
        lCand = lCand->suiv;
    }
    return compt;
}

bool videListe(ListeCand lCand) /* Permet de savoir si une liste est vide et retourne un booléen */
{
    if(lCand == NULL)
        return true;
    return false;
}

ListeCand rechercherCandListe(ListeCand lCand, int numeroC, int *trouve)  /* Permet de recherche un maillon dans une liste de départements passée en paramètre pour ensuite renvoyer son adresse*/
{
    if(videListe(lCand)) /* Si la liste est vide, la variable trouve passée en pointeur prend la valeur 0 et on retourne la liste */
    {
        *trouve = 0;
        return lCand;
    }
    if(numeroC < lCand->c->numeroC) /* Même procédé que la condition précédente si le département recherché n'est pas trouvé*/
    {
        *trouve = 0;
        return lCand;
    }
    if(numeroC == lCand->c->numeroC) /* Si le département recherché est trouvé, trouve prend la valeur 1 et on retourne la liste */
    {
        *trouve = 1;
        return lCand;
    }
    return rechercherCandListe(lCand->suiv, numeroC, trouve); /* Si aucune condition n'est respectée, renvoie la même recherche pour le maillon suivant */
}

Candidat** remplirCandCl(Candidat *tCand[], Candidat *tempC[], int tMax, int nbCl) /* Permet d'insérer une ville dans le tableau tiut et renvoie un entier dépendant du déroulement de l'insertion */
{
    int i, j, k = 0;
    for(i = 0; i < tMax; i++) /* On décale tous les éléments vers la droite depuis le dernier élément du tableau jusqu'à la position d'insertion passée en paramètre */
    {
        for(j = 0; j < tCand[i]->nombreChoix; j++)
        {
            if(strcmp(tCand[i]->tChoix[j]->ville, "Clermont-Ferrand") == 0 && strcmp(tCand[i]->tChoix[j]->dep, "Informatique") == 0)
            {
                tempC[k] = tCand[i];
                k = k + 1;
            }
        }
    }
    triTemp(tempC, nbCl);
    return tempC;
}

void triTemp(Candidat *tCand[], int nbCl) /* Fonction permettant de trier un tableau de candidats en effectuant un tri par échange */
{
    int mini;
    while(nbCl > 1)
    {
        mini = plusGrandCandTemp(tCand, nbCl); /* Appel de la fonction plusGrand pour déterminer le plus grand élément du tableau */
        echangerCand(tCand, mini, nbCl - 1); /* Appel de la fonction échange pour placer le plus grnad élément du tableau en dernière position */
        nbCl = nbCl - 1;
    }
}

int plusGrandCandTemp(Candidat *tCand[], int nbCl) /* Fonction permettant de trouver le plus grand élément du tableau candidat en le parcourant et en*/
{                                              /* comparant ses éléments. Elle renvoie ensuite l'indice du plus grand élément */
    int mini = 0, i;
    for(i = 1; i < nbCl; i++)
    {
        if(tCand[i]->moyenne < tCand[mini]->moyenne)
        {
            mini = i;
        }
    }
    return mini;
}

void afficherCandidatsParCategorie(FileCand f,ListeCand l)
{
    int categ, i, pos, trouve;
    printf("Quelle catégorie de candidats souhaitez-vous afficher ? (admis = 1 / attente = 2)\nSaisie : ");
    scanf("%d%*c", &categ);
    printf("\n");
	if(categ==1)
	{
		afficherListeCand(l);
	}
	if(categ == 2)
	{
		afficher(f);
	}
}

void afficherListeCand(ListeCand l)
{
	while(l->suiv!=NULL)
	{
		afficherCandidat(l->c);
		l = l->suiv;
	}
}
